## base image
#FROM node:12.2.0-alpine
#
## set working directory
#WORKDIR /app
#
## add `/app/node_modules/.bin` to $PATH
#ENV PATH /app/node_modules/.bin:$PATH
#
## install and cache app dependencies
#COPY .  /app/
#RUN npm install
#RUN npm install react-scripts@3.0.1 -g
#
## start app
#CMD ["npm", "start"]
# Stage 1
#FROM node:8 as react-build
#WORKDIR /app
#COPY . ./
#RUN yarn
#RUN yarn build
#
## Stage 2 - the production environment
#FROM nginx:alpine
#COPY nginx.conf /etc/nginx/conf.d/default.conf
#COPY --from=react-build /app/build /usr/share/nginx/html
#EXPOSE 80
#CMD ["nginx", "-g", "daemon off;"]

FROM mhart/alpine-node:11 AS builder
WORKDIR /app
COPY . .
RUN npm install
#RUN npm install react-scripts@3.0.1 -g
RUN npm run build

FROM mhart/alpine-node
RUN npm global add serve
WORKDIR /app
COPY --from=builder /app/build .
CMD ["serve", "-p", "80", "-s", "."]
