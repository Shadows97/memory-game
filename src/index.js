import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import EndGame from "./game/components/EndGame";
import Debut from './game/components/Debut'
import { Route, Link,Switch, BrowserRouter as Router } from 'react-router-dom'
import Home from './game/components/Home'



const route = (
    <Router>
        <div>
        <Switch>
            <Route path ='/Game/:token' component ={App}></Route>
            <Route path ='/Debut/:token' component ={Debut} ></Route>
            <Route path = '/fin/:token' component = {EndGame}></Route>
            <Route path = '/:token' component = {Home} ></Route>
            <Route path='/return' component={() => { 
     window.location = 'https://google.com'; 
     return null;
}}/>

            </Switch>
        </div>
 </Router>
);

ReactDOM.render(route, document.getElementById('root'));
