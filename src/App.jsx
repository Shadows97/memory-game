import React, { useState } from 'react';
import { ThemeProvider, createGlobalStyle } from 'styled-components';
import { getFromTheme } from './utils';
import { CookiesProvider } from 'react-cookie';
import './index.css';

import Game from './game';
import themes from './config/themes.json';


function App ({match}) {
  const [themeName, toggleTheme] = useTheme('darkTheme');

  const GlobalStyle = createGlobalStyle`
    body {
        background : -webkit-linear-gradient(left,#348ac7,#7474bf);
        color: ${getFromTheme('body.color')};
        transition: background .3s ease;
    }
  `;

  
  return (
    <CookiesProvider>
    <ThemeProvider theme={themes[themeName]}>
      <>
        <GlobalStyle />
        <Game toggleTheme={toggleTheme} token={match.params.token}/>
      </>
    </ThemeProvider>
    </CookiesProvider>
  );
}

function useTheme(defaultThemeName) {
  const [themeName, setTheme] = useState(defaultThemeName);

  function switchTheme(name) {
    setTheme(themeName === 'darkTheme' ? 'lightTheme' : 'darkTheme');
  }

  return [themeName, switchTheme];
}


export default App;
