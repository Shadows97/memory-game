import React, { memo, useReducer, useMemo, useEffect, useState } from 'react';

// import levels from '../config/levels';
import { Field } from './components/GameField';
import { GameFieldView, GameView, SwitchView } from './components/Styled';
import {
    GameReducer, initialState, NEW_LEVEL,
    FIELD_HIDE, FIELD_SHOW, RESET_LEVEL,
} from './game.reducer';
import { generateGameField } from './game.utils';
import Switch from 'rc-switch';
import { Redirect } from 'react-router-dom'

import 'rc-switch/assets/index.css';
import Chronometer from './components/Timer'
import { useCookies } from 'react-cookie';
import Data from './components/data';
import levels from '../config/levels';


function Game ({ toggleTheme, token }) {
    const [cookies, setCookie] = useCookies(['score','field','hiddenCells','level', 'showHidden', 'showField', 'levelConfig']);
   
    const [{ level, showHidden, showField, levelConfig }, dispatch] = useReducer(
        GameReducer, initialState 
    );


    // const levelConfig = levels[level];
    const { cellCount, memoryCount } = levelConfig;
    
    
    const [stop, setStop] = useState(0);
    //evel = cookies.level !== 0 ? cookies.level : level
    let times = '';
    let [timeTab, settimeTab] = useState(-1);
    let test = new Data();
    

    let { field, hiddenCells } = useMemo(
        () => generateGameField(cellCount, memoryCount),
        [levelConfig]
    );

  /*  console.log('field', field);
    console.log('hide', hiddenCells);*/

    useEffect(

        () => {
            
            
            setTimeout(dispatch, 500, { type: FIELD_SHOW })
            console.log(cookies);
            
        
        },
        [ levelConfig,field],
    );

   



    function updateLevel(shouldReset) {
        if (!shouldReset) {
        
             //setCookie('level', newData.level, { path: '/' });
             //setCookie('showHidden', newData.showHidden, { path: '/' });
            // setCookie('showField', newData.showField, { path: '/' });
             //setCookie('levelConfig', newData.levelConfig, { path: '/' });
             //setCookie('field', newField.field, { path: '/' });
             //setCookie('hiddenCells', newField.hiddenCells, { path: '/' });
             if (timeTab === -1) {
                settimeTab(1)
             } else {
                settimeTab(timeTab += 1);
             }
             
        }
        dispatch({ type: FIELD_HIDE });
        setTimeout(dispatch, 500, { type: shouldReset ? RESET_LEVEL : NEW_LEVEL, level: level + 1 });
    }
    function stopGame(time,stockTime) {
        times = stockTime;
        if (time === 4) {
            console.log('time ' + time);
            setStop(1);
            console.log(stop);
            
        }

    }
    function sendScore(token) {
        const formData = new FormData();
        formData.append('json', JSON.stringify('{score: ' + timeTab + '\n}'));
        var myHeaders = new Headers({
            'Access-Control-Allow-Origin': '*',
            'Accept': 'application/json',
            'Content-Type': 'application/json'
          });
        
    
        return fetch('http://candidatures.groupecerco.com:5000/'+ token + '/memory/' + timeTab, {
            method: 'POST',
            mode: 'no-cors',
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: formData //JSON.parse(JSON.stringify('{score: ' + timeTab + '\n}'))
        }).then(response => response.json())
    }

    
        if (stop === 1) {
            sendScore(token).then((json) => {
                console.log(json);
             })
            .catch(error => error);;
            return (<Redirect to={'/fin/' + token} />);
        }else if (stop === 2) {
            return (<Redirect to='/return' />);
        } else {
            return (<GameView>
                <GameFieldView {...levelConfig}>
                    <SwitchView>
                        <div>Niveau: {level}</div>
                        <div>
                            Theme mode: <Switch onClick={toggleTheme} />
                        </div>
                        <Chronometer time = {stopGame} />
                    </SwitchView>
                    <Field
                        {...levelConfig}
                        levelConfig={levelConfig}
                        visible={showField}
                        key={field}
                        level={level}
                        field={field}
                        hiddenCells={hiddenCells}
                        dispatch={dispatch}
                        showHidden={showHidden}
                        updateLevel={updateLevel}
                        
                    />
                </GameFieldView>
            </GameView>);
        }
}

export default memo(Game);
