export function generateGameField(cellCount, memoryCount) {
    const cellsIndexes = [...Array(cellCount * cellCount)]
        .map((_, i) => i);
       /* console.log("cell "+cellsIndexes);*/
    const field = [...cellsIndexes].fill(1);
    /*console.log("field "+field);*/
    const hiddenCells = [];

    for (let i = 0; i < memoryCount; i++) {
        const rNum = Math.floor(Math.random() * cellsIndexes.length);
        const toChange = cellsIndexes.splice(rNum, 1).pop();
        hiddenCells.push(toChange);
        /*field[toChange] = 2;*/
    }

    return {
        field, hiddenCells,
    };
}

export function changeState(hiddenCells, fields, indice) {
    let stopLoop = false;
    

    
    for (let i = 0 ; i < hiddenCells.length; i++){
        if (fields[hiddenCells[i]] === 2 ) {
            fields[hiddenCells[i]] = 5;
        }else if (fields[hiddenCells[i]] !== 2 && indice !== i && fields[hiddenCells[i]] !== 5)  {
            fields[hiddenCells[i]] = 2;
            stopLoop = true;
            indice = i;
            if (stopLoop) break;
        }

        }
        console.log('indice ' + indice);
    return {fields,indice};
}


export const WRONG_GUESSED_CELL = 0;
export const CORRECT_GUESSED_CELL = 3;
export const CELL = 1;
export const HIDDEN_CELL = 2;
