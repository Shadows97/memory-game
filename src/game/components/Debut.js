import React, { Component } from 'react'

class Debut extends Component {
    tok = this.props.match.params.token
    constructor(props) {
        super(props)
        this.state = {
            token: this.props.match.params.token,
        }
        console.log(this.props.match.params.token)
        this.onRedirect = this.onRedirect.bind(this);
    }

    onRedirect() {

        fetch('http://candidatures.groupecerco.com:5000/' + this.props.match.params.token)
            .then(res => res.json())
            .then(json => {

                console.log(json);
                if (json.status === 200) {
                    fetch('http://candidatures.groupecerco.com:5000/' + this.props.match.params.token + '/memory')
                        .then(res => res.json())
                        .then(json => {

                            console.log(json);
                            if (json.status === 200) {
                                window.location = "http://candidatures.groupecerco.com:3000/Game/" + this.props.match.params.token;
                            } else {
                                window.location = "http://candidatures.groupecerco.com:3000/fin/" + this.props.match.params.token;
                            }

                        })
                } else {
                    window.location = "http://candidatures.groupecerco.com:3000/fin/" + this.props.match.params.token;
                }

            })

    }


    render() {
        return ( <fragment >
            <nav className = "nav wrapper indigo darken-4" >
            <div class = "container" >
            <h1> TEST DE MÉMOIRE </h1>

            </div> </nav>

            <div className = "container" >

            <p></p>


            </div>

            <div className="container">

           <h4>NOTE</h4>

           
           <section>
          
           <li>
              On te conseille de désactiver tout bloqueur de publicité qui pourrait empêcher l'exécution de Javascript.
           </li>
           
           </section>
           
           </div>

          
            <div className = "container" >

           <section>
           <p></p>
           <div>
            <a class = "waves-effect waves-light btn-large "
            onClick = { this.onRedirect } >
            DÉMARRER LE JEU </a>
            </div>

            </section>


          </div>

            </fragment>
        )
    }
}

export default Debut
