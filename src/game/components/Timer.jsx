import React, { Component } from 'react';
import { instanceOf } from 'prop-types';
import { withCookies, Cookies } from 'react-cookie';

/*import ButtonStartStop from './ButtonStartStop'
import ButtonReset from './ButtonReset'*/

class Chronometer extends Component {
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
      };
    constructor(props){
        super(props);
        const { cookies } = props;
        console.log(new Date().getSeconds);
        this.state =  {
            isStartOrStop: true,
            timerID: 0,

            startTime: null,
            diff: +cookies.get('second', { path: '/'}) || 0,
            suspended: 0,
            minute: +cookies.get('minute', { path: '/'}) || 0,
            seconde: 0,

            disabled: false
        };
        cookies.remove('minute', { path: '/'});
        cookies.remove('second', { path: '/'});

        this.handleClickStartStop = this.handleClickStartStop.bind(this);
        this.handleClickReset = this.handleClickReset.bind(this);
       }

        componentDidMount() {
            this.handleClickStartStop()
            ;
        }


    tick() {
        const { cookies } = this.props;
        const data = new Date();
        data.setSeconds(data.getSeconds + 10);
        cookies.remove('minute', { path: '/'});
        cookies.remove('second', { path: '/'});
        this.setState(prevState => ({
            /*diff: new Date(new Date() - this.state.startTime)*/
            minute: this.state.diff <= 58 ? this.state.minute : this.state.minute  + 1,
            diff: this.state.diff === 59 ? 0 : this.state.diff  + 1 ,
            seconde: this.state.diff


        }));
        
        //cookies.set('minute', this.state.minute, { path: '/'});
        
        //cookies.set('second', +this.state.diff, { path: '/'});
        console.log(+cookies.get('second'));


    }

    componentWillUnmount() {
        const { cookies } = this.props;
        cookies.remove('minute', { path: '/'});
        cookies.remove('second', { path: '/'});
    }

    handleClickStartStop() {

        this.setState(prevState => ({
            isStartOrStop: !prevState.isStartOrStop
        }));


        const isStarted = this.state.isStartOrStop;
        let disabled = this.state.disabled;

        //Start
        if(isStarted){
            const { cookies } = this.props;
            this.setState({
                
                startTime: new Date() - this.state.suspended,
                timerID : setInterval(() => {
                    cookies.remove('minute', { path: '/'});
                    cookies.remove('second', { path: '/'});
                    this.tick()}, 1000),
                suspended: 0,
                disabled: false
            });
        }
        //Stop
        else{
            clearInterval(this.state.timerID);
            this.setState({
                startTime: null,
                suspended: this.state.diff,
                disabled: true
            });
        }
    }

    handleClickReset(){
        this.setState({
            startTime: null,
            diff: null,
            suspended: 0,
            disabled: false
        })
    }


    twoDigits(n){
        return n < 10 ? '0' + n : n;
    }

    componentWillUnmount() {
        clearInterval(this.state.timerID);
            this.setState({
                startTime: null,
                suspended: this.state.diff,
                disabled: true
            });
    }
    stop() {
        this.props.time(this.state.minute, this.state.minute + 'm' + this.state.seconde + 's, ');
       // console.log(this.state.minute + 'm' + this.state.seconde + 's');
    }


    render(){
        let diff = this.state.diff;
        let msec = 0;
        let sec = diff ? diff: 0;
        let min =  this.state.minute;
        /* let diff = this.state.diff;
        let msec = diff ? Math.round(diff.getMilliseconds()/10) : 0;
        let sec = diff ? diff.getSeconds() : 0;
        let min = diff ? diff.getMinutes() : 0;*/

        return (
            <div onClick={this.stop()}>
                {this.twoDigits(min)}:{this.twoDigits(sec)}{/*:{this.twoDigits(msec)}*/}
                &nbsp;
                {/*<ButtonStartStop  click={this.handleClickStartStop} value={this.state.isStartOrStop ? 'Start' : 'Stop'} />
                &nbsp;
                <ButtonReset  click={this.handleClickReset} value='Reset' disabled={this.state.disabled}/>*/}
            </div>
        )
    }

}

export default withCookies(Chronometer);
