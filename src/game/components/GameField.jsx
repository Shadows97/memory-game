import React, { memo, useState, useEffect } from 'react';
import styled from 'styled-components';



import { HIDDEN_CELL_HIDE, HIDDEN_CELL_SHOW } from '../game.reducer';
import { Cell } from './Cell';
import {WRONG_GUESSED_CELL, CORRECT_GUESSED_CELL, changeState} from '../game.utils';


const FieldView = styled.div`
    width: 100%;
    height: 100%;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    margin: 20px 0;
    backgroud : -webkit-linear-gradient(left,#3931af,#00c6ff);

    opacity: ${({ animationState }) => animationState};
    transform: scale(${({ animationState }) => animationState});
    transition: opacity .2s ease, transform .3s ease;
`;
let clik = [];
let count = 0;

export const Field = memo(function Field ({
    fieldSize = 0,
    cellCount = 0,
    space = 0,
    field = [],
    hiddenCells = [],
    level = 0,
    showHidden = false,
    dispatch,
    updateLevel,
    visible,
    levelConfig,
}) {
    const cellSize = fieldSize / cellCount - space;

    const { gameField, onCellClick } = useGameField(field, hiddenCells, updateLevel);
    const [items, setItem] = useState(null)
    



    useEffect(
        () => {
                
                console.log(hiddenCells);
                console.log(field);
       
                setTimeout(() => {
                    if (count < hiddenCells.length ) {
                        let data = changeState(hiddenCells,field,items);
                        setItem(data.indice);
                        field = data.fields;
                        dispatch({ type: HIDDEN_CELL_SHOW });

                        setTimeout(() => {
                            dispatch({ type: HIDDEN_CELL_HIDE });
                            count = count + 1;
                            console.log('setIntervale count ' + count);
                        }, 500);
                    }else {
                        count = 0;
                    }

                },900);

            

        },
        [levelConfig, field]
    );



    return (
        <FieldView
            animationState={visible ? 1 : 0}
            onClick={!showHidden ? onCellClick : null}>
            {
                gameField.map((cellValue, i) => (
                    <Cell
                        size={cellSize}
                        space={space}
                        key={i}
                        id={i}
                        hidden={hiddenCells}
                        value={cellValue}
                        forceShowHidden={showHidden} />
                        
                        
                ))
            }
        </FieldView>
    );
});

function useGameField(field, hiddenCells, updateLevel) {
    const [gameField, setField] = useState(field);
    const [gameHiddenCells, setHidden] = useState(hiddenCells);

    function onCellClick({ target }) {
        const id = Number(target.id);

        clik.push(id);
        if (hiddenCells.includes(id)  /* && hiddenCells.indexOf(id) === clik.indexOf(id) */ ) {
            const updatedField = gameField.map((e, i) => i === id ? CORRECT_GUESSED_CELL : e);
            const updatedHidden = gameHiddenCells.filter(e => e !== id);

            setField(updatedField);
            setHidden(updatedHidden);
            /*console.log(clik);*/
            if (clik.length === hiddenCells.length) {
                clik = [];
            }
            return !updatedHidden.length && setTimeout(updateLevel, 1000);
        }

        const updatedField = gameField.map((e, i) => i === id ? WRONG_GUESSED_CELL : e);
        setField(updatedField);
        /*console.log(clik);*/
        clik = [];
        return setTimeout(updateLevel, 1000, true);
        
    }

    return { gameField, onCellClick };
}
