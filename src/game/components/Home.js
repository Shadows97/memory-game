import React, { Component } from 'react'

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            token: this.props.match.params.token,
        };
        console.log(this.props.match.params);
        this.redirect = this.redirect.bind(this);

    }
    redirect() {
        window.location = 'http://candidatures.groupecerco.com:8080/?id=' + this.state.token;
    }

    render() {

        return (

            <
            fragment >
            <
            nav className = "nav wrapper blue darken-3" >
            <
            div class = "container" >
            <
            h3 > ANNONCES < /h3>

            <
            /div> <
            /nav>

            <
            div className = "container" >

            <
            h2 > JEUX < /h2>

            <
            p > Les deux tests suivants nous permettront autant qu 'à toi de savoir si la pédagogie de CERCO t'
            est adaptée. <
            /p> <
            p > Pas de panique, tout va bien se passer!Ci dessous quelques informations précieuses à lire avant de cliquer sur "Démarrer".


            <
            /p> <
            section >
            <
            li >
            Le premier test durera 4 minutes et pour le second test tu as besion d 'être disponible pendant 2 heures.

            <
            /li> <
            li >
            Nous te conseillons de faire les tests sur un ordinateur et non sur un téléphone portable ou une tablette. <
            /li> <
            li >
            ATTENTION: l 'accès aux tests se fait une seul fois, tu ne pourras pas retenter ta chance. <
            /li> <
            li >
            Il est impossible de faire une pause pendant cette étape. <
            /li> <
            li >
            En l 'absence de consigne, pas de problème, on sait que tu vas y arriver. <
            /li> <
            li >
            On te conseille de désactiver tout bloqueur de publicité qui pourrait empêcher l 'exécution de Javascript. <
            /li>

            <
            /section> <
            p >
            Prends ces 2 heures dans un lieu tranquille, souffle un bon coup et tout va bien se passer. <
            /p>


            <
            /div>

            <
            div className = "container" >

            <
            p > Après avoir appuyé sur "Démarrer", une autre fenêtre va s 'ouvrir, à la fin du jeu tu pourras retourner sur cette page afin de démarrer le Test 2.</p>



            <
            /div>


            <
            div className = 'container' >
            <
            h5 > Test 1 < /h5> <
            p > Ce jeu testera ta capacité de mémoire. < /p> <
            p > Ce jeu te prendra 4 minutes. < /p>

            <
            a class = "waves-effect waves-light btn-large "
            href = { '/Debut/' + this.state.token } >
            DÉMARRER LE TEST <
            /a>


            <
            /div>


            <
            div className = 'container' >
            <
            h5 > Test 2 < /h5> <
            p > Ce jeu testera ta capacité de logique. < /p> <
            p > Ce jeu te prendra 2 heures. < /p>

            <
            a class = "waves-effect waves-light btn-large"
            onClick = { this.redirect } >
            DÉMARRER LE TEST <
            /a>


            <
            /div> <
            div class = "col s12" >
            <
            h5 > < /h5> <
            p > < /p> <
            p > < /p>

            <
            a href = { '/Debut/' + this.state.token } >

            <
            /a>

            <
            /div>


            <
            footer class = "page-footer blue darken-3" >
            <
            div class = "container" >
            <
            div class = "row" >
            <
            div class = "col l6 s12" >
            <
            h5 class = "white-text" > < /h5> <
            p class = "grey-text text-lighten-4" > < /p> <
            /div> <
            div class = "col l4 offset-l2 s12" >
            <
            h5 class = "white-text" > < /h5> <
            ul >
            <
            li > < a class = "grey-text text-lighten-3"
            href = "#!" > < /a></li >

            <
            /ul> <
            /div> <
            /div> <
            /div> <
            div class = "footer-copyright" >
            <
            div class = "container" > ©1998 - 2019 GROUPE CERCO - Tous droits réservés. <
            a class = "grey-text text-lighten-4 right"
            href = "#!" > < /a> <
            /div> <
            /div> <
            /footer>

            <
            /fragment>
        )
    }
}

export default Home
